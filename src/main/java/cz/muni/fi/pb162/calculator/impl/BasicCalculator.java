package cz.muni.fi.pb162.calculator.impl;

import cz.muni.fi.pb162.calculator.Calculator;
import cz.muni.fi.pb162.calculator.Result;

/**
 * @author Dan Polansky (433716) on 24. 10. 2015.
 */
public class BasicCalculator implements Calculator {

    protected String[] operatorAndArguments;

    @Override
    public Result eval(String input) {
        operatorAndArguments = input.split(" ");
        String operator = operatorAndArguments[0];
        double number1;
        double number2;

        try {
            switch (operator) {
                case SUM_CMD:
                    if (operatorAndArguments.length == 3) {
                        number1 = Double.parseDouble(operatorAndArguments[1]);
                        number2 = Double.parseDouble(operatorAndArguments[2]);
                        return sum(number1, number2);
                    } else {
                        break;
                    }
                case SUB_CMD:
                    if (operatorAndArguments.length == 3) {
                        number1 = Double.parseDouble(operatorAndArguments[1]);
                        number2 = Double.parseDouble(operatorAndArguments[2]);
                        return sub(number1, number2);
                    } else {
                        break;
                    }
                case MUL_CMD:
                    if (operatorAndArguments.length == 3) {
                        number1 = Double.parseDouble(operatorAndArguments[1]);
                        number2 = Double.parseDouble(operatorAndArguments[2]);
                        return mul(number1, number2);
                    } else {
                        break;
                    }
                case DIV_CMD:
                    if (operatorAndArguments.length == 3) {
                        number1 = Double.parseDouble(operatorAndArguments[1]);
                        number2 = Double.parseDouble(operatorAndArguments[2]);
                        return div(number1, number2);
                    } else {
                        break;
                    }
                case FAC_CMD:
                    if (operatorAndArguments.length == 2) {
                        int facNum = Integer.parseInt(operatorAndArguments[1]);
                        return fac(facNum);
                    } else {
                        break;
                    }
                default:
                    return new CalculationResult(UNKNOWN_OPERATION_ERROR_MSG);
            }
        } catch (NumberFormatException nfe) {
            return new CalculationResult(WRONG_ARGUMENTS_ERROR_MSG);
        }
        return new CalculationResult(UNKNOWN_OPERATION_ERROR_MSG);
    }


    @Override
    public Result sum(double x, double y) {
        return new CalculationResult(x + y);
    }

    @Override
    public Result sub(double x, double y) {
        return new CalculationResult(x - y);
    }

    @Override
    public Result mul(double x, double y) {
        return new CalculationResult(x * y);
    }

    @Override
    public Result div(double x, double y) {
        if (y == 0) {
            return new CalculationResult(COMPUTATION_ERROR_MSG);
        } else {
            return new CalculationResult(x / y);
        }
    }

    @Override
    public Result fac(int x) {
        if (x < 0) {
            return new CalculationResult(COMPUTATION_ERROR_MSG);
        } else {
            double factX = 1;
            for (int i = 1; i <= x; i++) {
                factX *= i;
            }
            return new CalculationResult(factX);
        }
    }
}
