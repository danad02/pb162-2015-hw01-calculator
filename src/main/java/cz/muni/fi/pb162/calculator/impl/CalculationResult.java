package cz.muni.fi.pb162.calculator.impl;

import cz.muni.fi.pb162.calculator.Calculator;
import cz.muni.fi.pb162.calculator.Result;

/**
 * @author Dan Polansky (433716) on 24. 10. 2015.
 */
public class CalculationResult implements Result {

    private Double numeric;
    private String alphanumeric;

    CalculationResult (double numeric) {
        this.numeric = numeric;
        this.alphanumeric = null;
    }

    CalculationResult (String alphanumeric) {
        this.numeric = null;
        this.alphanumeric = alphanumeric;
    }

    @Override
    public boolean isSuccess() {
        if (this.alphanumeric == null) {
            return true;
        } else {
            switch (this.alphanumeric) {
                case Calculator.COMPUTATION_ERROR_MSG:
                case Calculator.UNKNOWN_OPERATION_ERROR_MSG:
                case Calculator.WRONG_ARGUMENTS_ERROR_MSG:
                    return false;
                default:
                    break;
            }
        }
        return true;
    }

    @Override
    public boolean isAlphanumeric() {
        return (this.alphanumeric != null);
    }

    @Override
    public boolean isNumeric() {
        return (this.numeric != null);
    }

    @Override
    public double getNumericValue() {
        if (isSuccess() && isNumeric()) {
            return this.numeric;
        } else {
            return Double.NaN;
        }
    }

    @Override
    public String getAlphanumericValue() {
        if (isAlphanumeric()) {
            return this.alphanumeric;
        } else {
            return null;
        }
    }
}
