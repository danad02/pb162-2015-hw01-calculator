package cz.muni.fi.pb162.calculator.impl;

import cz.muni.fi.pb162.calculator.Calculator;
import cz.muni.fi.pb162.calculator.NumeralConverter;

/**
 * @author Dan Polansky (433716) on 27. 10. 2015.
 */
public interface ConvertingCalculator extends Calculator, NumeralConverter {

}
