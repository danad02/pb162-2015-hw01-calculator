package cz.muni.fi.pb162.calculator.impl;

import cz.muni.fi.pb162.calculator.Result;

/**
 * @author Dan Polansky (433716) on 27. 10. 2015.
 */
public class AdvancedCalculator extends BasicCalculator implements ConvertingCalculator {

    /**
     * Evaluate textual input and perform computation (extends method eval from BasicCalculator)
     * @param input input stringg
     * @return result
     */
    public Result eval(String input) {
        operatorAndArguments = input.split(" ");
        String operator = operatorAndArguments[0];
        int base;
        int intNumber;
        String strNumber;

        try{
            switch (operator) {
                case TO_DEC_CMD:
                    if (operatorAndArguments.length == 3) {
                        base = Integer.parseInt(operatorAndArguments[1]);
                        strNumber = operatorAndArguments[2];
                        return toDec(base, strNumber);
                    } else {
                        break;
                    }

                case FROM_DEC_CMD:
                    if (operatorAndArguments.length == 3) {
                        base = Integer.parseInt(operatorAndArguments[1]);
                        intNumber = Integer.parseInt(operatorAndArguments[2]);
                        return fromDec(base, intNumber);
                    } else {
                        break;
                    }

                default:
                    return super.eval(input);
            }
        } catch (NumberFormatException nfe) {
            return new CalculationResult(WRONG_ARGUMENTS_ERROR_MSG);
        }
        return new CalculationResult(UNKNOWN_OPERATION_ERROR_MSG);
    }

    /**
     * inBounds checks, if base of the numeral system is in bounds
     * @param base is base of the numeral system
     * @return true if base is in the bounds, false otherwise
     */
    private boolean inBounds(int base) {
        return (base >= 2 && base <= 16);
    }

    @Override
    public Result toDec(int base, String number) {
        if (!inBounds(base)) {
            return new CalculationResult(COMPUTATION_ERROR_MSG);
        }

        int numericResult = 0;
        int length = number.length() - 1;
        int isDigit;

        for (int i = length; i >= 0; i--) {
            if ((isDigit = (int) (DIGITS.indexOf(number.charAt(i)) * Math.pow(base, length - i))) >= 0) {
                numericResult += isDigit;
            } else {
                return new CalculationResult(COMPUTATION_ERROR_MSG);
            }
        }
        return new CalculationResult(numericResult);
    }

    @Override
    public Result fromDec(int base, int number) {
        if (!inBounds(base)) {
            return new CalculationResult(COMPUTATION_ERROR_MSG);
        }

        String result = "";

        if (number == 0) {
            return new CalculationResult("0");
        }

        while (number > 0) {
            result = DIGITS.charAt(number % base) + result;
            number /= base;
        }
        return new CalculationResult(result);
    }
}
